# Clouddurchsucher nach Muster
# Edlir Murati
# 14.07.2023

#!/bin/bash

# Muster zum suchen, Variablen festlegen
muster1="edlir"
muster2="murati"

# Verzeichnis das durchsucht werden soll, Variablen festlegen
verzeichnis="/home/edlir/OneDrive"

# email-einstellungen, Variablen festlegen
email_empfaenger="edlir.murati01@gmail.com"
email_betreff="Sicherheitswarnung"
email_nachricht="Im Anhang sind die Dateien mit '$muster1' oder '$muster2' im Namen oder Inhalt."

# Suchen der Dateien nach Mustern und Löschen, Variable festlegen
gefundene_dateien=$(grep -rl -e "$muster1" -e "$muster2" "$verzeichnis")

# email mit den gefundenen Dateien senden
if [ -n "$gefundene_dateien" ]; then
    echo "$email_nachricht" | mail -s "$email_betreff" -A "$gefundene_dateien" "$email_empfaenger"
    echo "Eine E-Mail mit den gefundenen Dateien wurde an $email_empfaenger gesendet."

    # löschen der gefundenen Dateien
    while IFS= read -r datei; do
        rm "$datei"
        echo "Die Datei $datei wurde gelöscht."
    done <<< "$gefundene_dateien"
else
    echo "Keine Dateien gefunden, keine E-Mail gesendet."
fi

echo "Benachrichtigt."
