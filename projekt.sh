# Clouddurchsucher nach Muster
# Edlir Murati
# 14.07.2023

#!/bin/bash

# muster zum suchen, Variablen festlegen
muster1="edlir"
muster2="murati"

# verzeichnis das durchsucht werden soll, Variablen festlegen
verzeichnis="/home/edlir/OneDrive"

# email-Einstellungen, Variablen festlegen
email_empfaenger="edlir.murati01@gmail.com"
email_betreff="Sicherheitswarnung"
email_nachricht="Im Anhang sind die Dateien mit 'edlir' oder 'murati' im Namen."

# Suchen und Löschen der Dateien, variabeln festlegen
gefundene_dateien=$(find "$verzeichnis" -type f \( -name "*$muster1*" -o -name "*$muster2*" \) -print)

# email mit den gefundenen Dateien senden
if [ -n "$gefundene_dateien" ]; then
    echo "$email_nachricht" | mail -s "$email_betreff" -A "$gefundene_dateien" "$email_empfaenger"
    echo "Eine E-Mail mit den gefundenen Dateien wurde an $email_empfaenger gesendet."
else
    echo "Keine Dateien gefunden, keine E-Mail gesendet."
fi

for datei in $gefundene_dateien; do
    rm "$datei"
    echo "Die Datei $datei wurde gelöscht."
done

echo "Benachrichtigt."
